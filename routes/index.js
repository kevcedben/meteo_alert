var express = require('express'),
    http = require('http');

var parseString  = require('xml2js').parseString;
var url = "http://vigilance.meteofrance.com/data/NXFR49_LFPW_.xml";


var router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'skdsdfg Page'
  });
});

/* GET doc page */
router.get('/alert/api/docs', function(req, res, next) {
  res.sendfile('views/doc.html');
});

router.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

function xmlToJson(url, callback) {
  var req = http.get(url, function(res) {
    var xml = '';
    
    res.on('data', function(chunk) {
      xml += chunk;
    });

    res.on('error', function(e) {
      callback(e, null);
    }); 

    res.on('timeout', function(e) {
      callback(e, null);
    }); 

    res.on('end', function() {
      parseString(xml, function(err, result) {
        callback(null, result);
      });
    });
  });
}

router.get('/alert/:ID', function (req, res) {
  var ID = req.params.ID;
  var phenomeneArray = [];
  
  xmlToJson(url, function(err, data){
    var array = data.SIV_MENHIR.PHENOMENE;

    var obj = {
      AlertMeteo: []
    };

    var risqueArray = ["Pas de vigilance particulière.",
                       "Soyez attentifs.",
                       "Soyez très vigilant.",
                       "Une vigilance absolue s'impose."];

    var phenomeneArray = ["Vent",
                          "Pluie Innondation",
                          "Orages",
                          "Innondation",
                          "Neige",
                          "Canicule",
                          "Grand froid",
                          "Avalanches",
                          "Vagues-Submersion"];

    var departmentFound = false;
    
    if(/^([0-9][0-9])$/.test(ID)){
      array.forEach(function(element) {
        if(element.$.departement == ID){
          departmentFound = true;
          obj.AlertMeteo.push({departement: element.$.departement, 
                          phenomene: phenomeneArray[parseInt(element.$.phenomene,10) - 1], 
                          risque: risqueArray[parseInt(element.$.couleur,10) - 1]})
        }
      });
      if(departmentFound){
        res.status(200).send(obj);
      }else{
        res.status(404).send("Department not found");
      }    
    }else{
      res.status(400).send("Invalid Department ID supplied");
    } 

  });
});

module.exports = router;
